from django.contrib import admin
from music.models import Favorite, Song
# Register your models here.

admin.site.register(Song)
admin.site.register(Favorite)