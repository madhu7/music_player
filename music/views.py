from django.shortcuts import get_object_or_404, render
from .models import Song
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from music.forms import FavoriteForm, UserForm
from music.models import Favorite
from django.views.generic import DetailView, CreateView, DeleteView
# Create your views here.


def favorite(request , song_id):
    song = get_object_or_404(Song, pk = song_id)
    try: 
        if song.is_favorite:
            song.is_favorite = False
        else:
            song.is_favorite = True
        song.save()
    except(KeyError, Song.DoesNotExist):
        return JsonResponse({'success': False})
    else: 
        return JsonResponse({'success': True})

def index(request):
    return render(request, 'index.html')

@login_required
def special(request):
    return HttpResponse("You are logged in !")

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):
    registered = False
    if request.method == 'POST':
        print("post")
        user_form = UserForm(data = request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user = user.save()
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()
    return render(request,'registration.html',{'user_form':user_form,'registered':registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password=password)
        
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password:{}".format(username,password))
            return HttpResponse("Invalid login details given")
    else:
        return render(request,'login.html')

# def songs(request, filter_by):
#     if not request.user.is_authenticated():
#         return render(request, 'music/login.html')
#     else:
#         try:
#             song_ids = []
#             for album in Album.objects.filter(user=request.user):
#                 for song in album.song_set.all():
#                     song_ids.append(song.pk)
#             users_songs = Song.objects.filter(pk__in=song_ids)
#             if filter_by == 'favorites':
#                 users_songs = users_songs.filter(is_favorite=True)
#         except Album.DoesNotExist:
#             users_songs = []
#         return render(request, 'music/songs.html', {
#             'song_list': users_songs,
#             'filter_by': filter_by,
#         })

# 


class SongDetailsView(DetailView):
    model = Song
    template_name = 'song.html'
    context_object_name = 'song'
    

class FavoriteCreateView(CreateView):
    form_class = FavoriteForm
    http_method_names = ['post']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(FavoriteCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            data = {
                'status': True,
                'message': "Please login first",
                'redirect': None
            }
            return JsonResponse(data=data)
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

def favoriteunfavorite(request):
    if request.method == "POST":
        if request.POST.get('decision') == 'make':
            song = Song.objects.get(id=request.POST.get('song_id'))
            if not Favorite.objects.filter(user=request.user, song=song).exists():
                Favorite.objects.create(user=request.user, song=song)
                data = {
                    'status': True,
                    'message': "Song marked as favorite",
                    'redirect': None
                }
                return JsonResponse(data)
            else:
                data = {
                    'status': True,
                    'message': "Already favorite",
                    'redirect': None
                }

                return JsonResponse(data)
        else:
            song = Song.objects.get(id=request.POST.get('song_id'))
            Favorite.objects.filter(user=request.user, song=song).delete()
            data = {
                'status': True,
                'message': "Song unfavorited",
                'redirect': None
            }
            return JsonResponse(data)
    else:
        data = {
            'status': False,
            'message': "Method not allowed",
            'redirect': None
        }

        return JsonResponse(data)


class UnFavoriteView(DeleteView):
    model = Favorite

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        data = {
            'status': True,
            'message': "Song unfavorited.",
            'redirect': None
        }

        return JsonResponse(data)